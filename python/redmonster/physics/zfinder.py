# First pass redshift finder of redmonster.  Able to search entire
# parameter-space and redshift-space of models in increments
# of d(loglam)/d(pixel)
#
# Tim Hutchinson, University of Utah @ IAC, May 2014
# t.hutchinson@utah.edu

from os import environ, makedirs
from os.path import join, exists
from time import gmtime, strftime

import numpy as n
from astropy.io import fits
from scipy.optimize import nnls
import matplotlib as m
from matplotlib import pyplot as p

from redmonster.datamgr.ssp_prep import SSPPrep
from redmonster.physics.misc import poly_array, two_pad
from redmonster.datamgr.io2 import read_ndArch, write_chi2arr

# Assumes all templates live in $REDMONSTER_DIR/templates/

class ZFinder:
    
    def __init__(self, fname=None, npoly=None, zmin=None, zmax=None):
        self.fname = fname
        self.npoly = npoly if npoly else 4
        self.zmin = float(zmin)
        self.zmax = float(zmax)
        self.pixoffset = None
        self.zchi2arr = None
        try:
            self.templatesdir = environ['REDMONSTER_TEMPLATES_DIR']
        except KeyError as e:
            self.templatesdir = None
            print "Enviromental variable 'REDMONSTER_TEMPLATES_DIR' not set: \
                    %r" % e
        self.read_template()
        self.npars = len(self.templates.shape) - 1
        self.templates_flat = n.reshape(self.templates, (-1,self.fftnaxis1))
        self.t_fft = n.fft.fft(self.templates_flat)
        self.t2_fft = n.fft.fft(self.templates_flat**2)
        self.f_nulls = []
        self.chi2_null = []
        self.sn2_data = []
    

    def read_template(self):
        self.templates, self.baselines, self.infodict = \
                read_ndArch(join(self.templatesdir,self.fname))
        self.type = self.infodict['class']
        self.origshape = self.templates.shape
        self.ntemps = self.templates[...,0].size
        self.fftnaxis1 = two_pad(self.origshape[-1])
        self.tempwave = 10**(self.infodict['coeff0'] + \
                             n.arange(self.infodict['nwave']) * \
                             self.infodict['coeff1'])
        templates_pad = n.zeros( self.origshape[:-1]+(self.fftnaxis1,) )
        templates_pad[...,:self.origshape[-1]] = self.templates
        self.templates = templates_pad
    
    
    def create_z_baseline(self, loglam0):
        self.zbase = ((10**loglam0)/self.tempwave) - 1
    
    
    def conv_zbounds(self):
        zmaxpix = n.where( abs((self.zbase-self.zmin)) == \
                          n.min(abs(self.zbase-self.zmin)) )[0][0]
        zminpix = n.where( abs((self.zbase-self.zmax)) == \
                          n.min(abs(self.zbase-self.zmax)) )[0][0]
        return zminpix, zmaxpix
    
    
    def zchi2(self, specs, specloglam, ivar, npixstep=1, chi2file=False,
              plate=None, mjd=None, fiberid=None):
        self.chi2file = chi2file
        self.npixstep = npixstep
        self.zwarning = n.zeros(specs.shape[0])
        flag_val_unplugged = int('0b10000000',2)
        flag_val_nodata = int('0b1000000000',2)
        flag_val_neg_model = int('0b1000',2)
        self.create_z_baseline(specloglam[0])
        if (self.zmin != None) and (self.zmax != None) and \
                (self.zmax > self.zmin):
            bounds_set = True
            zminpix, zmaxpix = self.conv_zbounds()
            self.pixoffset = zminpix
            num_z = int(n.floor( (zmaxpix - zminpix) / npixstep ))
            zinds = zminpix + n.arange(num_z)*npixstep
            self.zbase = self.zbase[zinds]
        else:
            bounds_set = False
            # Number of pixels to be fitted in redshift
            num_z = int(n.floor( (zself.origshape[-1] - specs.shape[-1]) /
                                npixstep ))
        
        # Create arrays for use in routine
        # Create chi2 array of shape (# of fibers, template_parameter_1,
        # ..., template_parameter_N, # of redshifts)
        zchi2arr = n.zeros((specs.shape[0], self.templates_flat.shape[0],
                            num_z))
        temp_zwarning = n.zeros(zchi2arr.shape)
        # Compute poly terms, noting that they will stay fixed with
        # the data - assumes data is passed in as shape (nfibers, npix)
        polyarr = poly_array(self.npoly, specs.shape[1])
        pmat = n.zeros( (self.npoly+1, self.npoly+1, self.fftnaxis1),
                       dtype=float)
        bvec = n.zeros( (self.npoly+1, self.fftnaxis1), dtype=float)
        
        # Pad data and SSPs to a power of 2 for faster FFTs
        data_pad = n.zeros(specs.shape[:-1] + (self.fftnaxis1,), dtype=float)
        data_pad[...,:specs.shape[-1]] = specs
        ivar_pad = n.zeros(ivar.shape[:-1] + (self.fftnaxis1,), dtype=float)
        ivar_pad[...,:specs.shape[-1]] = ivar
        poly_pad = n.zeros((self.npoly, self.fftnaxis1), dtype=float)
        poly_pad[...,:polyarr.shape[-1]] = polyarr
        
        # Pre-compute FFTs for use in convolutions
        data_fft = n.fft.fft(data_pad * ivar_pad)
        ivar_fft = n.fft.fft(ivar_pad)
        poly_fft = n.zeros((ivar_pad.shape[0], self.npoly, self.fftnaxis1),
                           dtype=complex)
        for i in xrange(self.npoly):
            poly_fft[:,i,:] = n.fft.fft(poly_pad[i] * ivar_pad)
        
        # Compute z for all fibers
        
        for i in xrange(specs.shape[0]): # Loop over fibers
            print 'Fitting fiber %s of %s for template %s' % \
                    (i+1, specs.shape[0], self.fname)
            self.sn2_data.append (n.sum( (specs[i]**2)*ivar[i] ) )
            # If flux is all zeros, flag as unplugged according to BOSS
            # zwarning flags and don't bother with doing fit
            if len(n.where(specs[i] != 0.)[0]) == 0:
                self.zwarning[i] = int(self.zwarning[i]) | flag_val_unplugged
                self.f_nulls.append(0)
                self.chi2_null.append(0)
            elif len(n.where(ivar[i] !=0.)[0]) == 0:
                self.zwarning[i] = int(self.zwarning[i]) | flag_val_nodata
                self.f_nulls.append(0)
                self.chi2_null.append(0)
            else: # Otherwise, go ahead and do fit
                for ipos in xrange(self.npoly):
                    bvec[ipos+1] = n.sum( poly_pad[ipos] * data_pad[i] *
                                         ivar_pad[i])
                for ipos in xrange(self.npoly):
                    for jpos in xrange(self.npoly):
                        pmat[ipos+1,jpos+1] = n.sum( poly_pad[ipos] *
                                                    poly_pad[jpos] *ivar_pad[i])
                f_null = n.linalg.solve(pmat[1:,1:,0],bvec[1:,0])
                self.f_nulls.append( f_null )
                self.chi2_null.append( self.sn2_data[i] -
                                      n.dot(n.dot(f_null,pmat[1:,1:,0]),f_null))
                print 'Chi^2_null value is %s' % self.chi2_null[i]
                # Loop over templates
                for j in xrange(self.templates_flat.shape[0]):
                    pmat[0,0] = n.fft.ifft(self.t2_fft[j] *
                                           ivar_fft[i].conj()).real
                    bvec[0] = n.fft.ifft(self.t_fft[j]*data_fft[i].conj()).real
                    for ipos in xrange(self.npoly):
                        pmat[ipos+1,0] = pmat[0,ipos+1] = \
                                n.fft.ifft(self.t_fft[j] *
                                           poly_fft[i,ipos].conj()).real
                    if bounds_set:
                        for l in n.arange(num_z)*self.npixstep:
                            try:
                                f = n.linalg.solve(pmat[:,:,l+zminpix],
                                                   bvec[:,l+zminpix])
                                zchi2arr[i,j,(l/self.npixstep)] = \
                                        self.sn2_data[i] - \
                                        n.dot(n.dot(f,pmat[:,:,l+zminpix]),f)
                                if (f[0] < 0):
                                    temp_zwarning[i,j,
                                            (l/self.npixstep)] = \
                                            int(temp_zwarning[i,j,
                                                      (l/self.npixstep)]) | \
                                                            flag_val_neg_model
                                    zchi2arr[i,j,(l/self.npixstep)] = \
                                            self.chi2_null[i]
                                    try:
                                        #f = nnls(pmat[:,:,l+zminpix],
                                                  #bvec[:,l+zminpix])[0]
                                        #zchi2arr[i,j,(l/self.npixstep)] =
                                                #self.sn2_data - \
                                        n.dot(n.dot(f,pmat[:,:,l+zminpix]),f)
                                        pass
                                    except Exception as e:
                                        print "Except: %r" % e
                                        zchi2arr[i,j,(l/self.npixstep)] = \
                                                self.chi2_null[i]
                            except Exception as e:
                                print "Exception: %r" % e
                                zchi2arr[i,j,(l/self.npixstep)] = \
                                        self.chi2_null[i]
                    else:
                        for l in n.arange(num_z)*self.npixstep:
                            try:
                                f = n.linalg.solve(pmat[:,:,l],bvec[:,l])
                                zchi2arr[i,j,(l/self.npixstep)] = \
                                        self.sn2_data - \
                                                n.dot(n.dot(f,pmat[:,:,l]),f)
                                if (f[0] < 0.):
                                    temp_zwarning[i,j,(l/self.npixstep)] = \
                                            int(temp_zwarning[
                                                  i,j,(l/self.npixstep)]) | \
                                                        flag_val_neg_model
                                    zchi2arr[i,j,(l/self.npixstep)] = \
                                            self.chi2_null[i]
                                    try:
                                        #f = nnls(pmat[:,:,l],bvec[:,l])[0]
                                        #zchi2arr[i,j,(l/self.npixstep)] =
                                        #self.sn2_data - \
                                        n.dot(n.dot(f,pmat[:,:,l]),f)
                                        pass
                                    except Exception as e:
                                        print "Except: %r" % e
                                        zchi2arr[i,j,(l/self.npixstep)] = \
                                                self.chi2_null[i]
                            except Exception as e:
                                print "Exception: %r" % e
                                zchi2arr[i,j,(l/self.npixstep)] = \
                                        self.chi2_null[i]
        # Use only neg_model flag from best fit model/redshift and add
        # it to self.zwarning
        for i in xrange(self.zwarning.shape[0]):
            minpos = ( n.where(zchi2arr[i] == n.min(zchi2arr[i]))[0][0],
                      n.where(zchi2arr[i] == n.min(zchi2arr[i]))[1][0] )
            self.zwarning[i] = int(self.zwarning[i]) | \
                    int(temp_zwarning[i,minpos[0],minpos[1]])
        zchi2arr = n.reshape(zchi2arr, (specs.shape[0],) + self.origshape[:-1] +
                             (num_z,) )
        bestl = n.where(zchi2arr == n.min(zchi2arr))[-1][0]
        if bounds_set:
            thisz = ((10**(specloglam[0]))/self.tempwave[bestl+zminpix])-1
        else:
            thisz = ((10**(specloglam[0]))/self.tempwave[bestl])-1
        #return zchi2arr
        self.zchi2arr = zchi2arr
        #self.store_models(specs, ivar)
        if self.chi2file is True:
            if (plate is not None) & (mjd is not None) & (fiberid is not None):
                write_chi2arr(plate, mjd, fiberid, self.zchi2arr)
            else:
                print 'Plate/mjd/fiberid not given - unable to write chi2 file!'
        else:
            print 'Not writing chi2'


    def store_models(self, specs, ivar):
        self.models = n.zeros( (specs.shape) )
        for i in xrange(self.models.shape[0]):
            minloc = n.unravel_index( self.zchi2arr[i].argmin(),
                                     self.zchi2arr[i].shape )
            pmat = n.zeros( (specs.shape[-1],self.npoly+1) )
            this_temp = self.templates[minloc[:-1]]
            pmat[:,0] = this_temp[(minloc[-1]*self.npixstep) +
                                  self.pixoffset:(minloc[-1]*self.npixstep) +
                                  self.pixoffset+specs.shape[-1]]
            polyarr = poly_array(self.npoly, specs.shape[-1])
            pmat[:,1:] = n.transpose(polyarr)
            ninv = n.diag(ivar[i])
            try: # Some eBOSS spectra have ivar[i] = 0 for all i
                '''
                f = n.linalg.solve( n.dot(n.dot(n.transpose(pmat),ninv),pmat),
                                n.dot( n.dot(n.transpose(pmat),ninv),specs[i]) )
                '''
                f = nnls( n.dot(n.dot(n.transpose(pmat),ninv),pmat),
                         n.dot( n.dot(n.transpose(pmat),ninv),specs[i]) );\
                                f = n.array(f)[0]
                self.models[i] = n.dot(pmat, f)
            except Exception as e:
                self.models[i] = n.zeros(specs.shape[-1])
                print "Exception: %r" % r


    def write_chi2arr(self, plate, mjd, fiberid):
        prihdu = fits.PrimaryHDU(self.zchi2arr)
        thdulist = fits.HDUList([prihdu])
        try:
            rsr = environ['REDMONSTER_SPECTRO_REDUX']
            run2d = environ['RUN2D']
            run1d = environ['RUN1D']
            if (rsr is not None) & (run2d is not None) & (run1d is not None):
                testpath = join(rsr, run2d, '%s' % plate, run1d)
                if exists(testpath):
                    dest = testpath
                else:
                    try:
                        makedirs(testpath)
                        dest = testpath
                    except Exception as e:
                        print "Exception: %r" % e
                        dest = None
        except Exception as e:
            print "Exception: %r" % e
            dest = None
        if dest is not None:
            try:
                thdulist.writeto(join(dest, '%s' % 'chi2arr-%s-%s-%s-%03d.fits'
                                      % (self.type, plate, mjd, fiberid)),
                                 clobber=True)
                print 'Writing chi2 file to %s' % \
                        join(dest, '%s' % 'chi2arr-%s-%s-%s-%03d.fits' %
                             (self.type, plate, mjd, fiberid))
            except Exception as e:
                print "Exception: %r" % e
                print 'Environment variables not set or path does not exist - \
                        not writing chi2 file!'
        else:
            print 'Environment variables not set or path does not exist - \
                    not writing chi2 file!'





























